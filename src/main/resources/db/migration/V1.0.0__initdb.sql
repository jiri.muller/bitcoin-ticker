create table tick
(
    id          int8 not null,
    price       numeric(28, 3),
    timestamp   timestamp
);
create sequence hibernate_sequence start 1 increment 1;
