package cz.jirimuller.bitcointicker.model;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class TickDto {
    private BigDecimal price;
    private LocalDateTime timestamp;
}
