package cz.jirimuller.bitcointicker.api;

import cz.jirimuller.bitcointicker.model.TickDto;
import cz.jirimuller.bitcointicker.service.TickerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/ticks")
@RequiredArgsConstructor
public class TickerController {

    private final TickerService service;

    @GetMapping("current")
    public TickDto getCurrent() {
        return service.getCurrent();
    }

    @GetMapping("highest")
    public TickDto getHighest() {
        return service.getHighest();
    }

    @GetMapping("lowest")
    public TickDto getLowest() {
        return service.getLowest();
    }

    @GetMapping("{ageInTicks}")
    public TickDto getByAge(@PathVariable int ageInTicks) {
        return service.getByAge(ageInTicks);
    }

    @GetMapping
    public List<TickDto> getAll() {
        return service.getAll();
    }
}
