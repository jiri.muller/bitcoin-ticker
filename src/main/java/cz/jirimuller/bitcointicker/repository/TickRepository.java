package cz.jirimuller.bitcointicker.repository;

import cz.jirimuller.bitcointicker.domain.Tick;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Optional;

@Repository
public interface TickRepository extends JpaRepository<Tick, Long> {
    @Query(value = "SELECT * FROM tick c ORDER BY c.timestamp DESC LIMIT 1", nativeQuery = true)
    Optional<Tick> getCurrent();

    void deleteAllByTimestampBefore(LocalDateTime timestamp);

    @Query(value = "SELECT * FROM tick c WHERE c.TimeStamp = (SELECT cc.Timestamp FROM tick cc " +
            "ORDER BY cc.TimeStamp DESC OFFSET :ageInTicks LIMIT 1)", nativeQuery = true)
    Optional<Tick> getByAge(@Param("ageInTicks") int ageInTicks);

    @Query(value = "SELECT * FROM tick c ORDER BY c.price DESC LIMIT 1", nativeQuery = true)
    Optional<Tick> getHighest();

    @Query(value = "SELECT * FROM tick c ORDER BY c.price LIMIT 1", nativeQuery = true)
    Optional<Tick> getLowest();

}
