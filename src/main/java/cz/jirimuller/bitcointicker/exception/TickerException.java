package cz.jirimuller.bitcointicker.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class TickerException extends RuntimeException {
    private final HttpStatus status;

    public TickerException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }
}
