package cz.jirimuller.bitcointicker.exception;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ExceptionResponseDto {
    private int status;
    private String message;
}

