package cz.jirimuller.bitcointicker.exception;

import org.springframework.http.HttpStatus;

public class TickerNotFoundException extends TickerException {
    public TickerNotFoundException() {
        super("Price not found.", HttpStatus.NOT_FOUND);
    }
}
