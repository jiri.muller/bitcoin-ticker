package cz.jirimuller.bitcointicker.service;

import cz.jirimuller.bitcointicker.model.TickDto;

import java.util.List;

public interface TickerService {
    /**
     * Calls the Coindesk client and saves the latest value of BTC to the database.
     * If the database contains records older than the amount of hours
     * specified in deleteRecordsAfterHours in the config, these are discarded.
     */
    void saveCurrent();

    /**
     * Retrieves the latest remembered price of BTC from the database.
     * @return      the current price of BTC and its timestamp of retrieval
     */
    TickDto getCurrent();

    /**
     * Retrieves the highest remembered price of BTC from the database.
     * @return      the highest price of BTC and its timestamp of retrieval
     */
    TickDto getHighest();

    /**
     * Retrieves the lowest remembered price of BTC from the database.
     * @return      the lowest price of BTC and its timestamp of retrieval
     */
    TickDto getLowest();

    /**
     * Retrieves the price of BTC a specific amount of ticks ago.
     * @param ageInTicks    the age in ticks of the price point to be retrieved
     * @return              the price and its timestamp ageInTicks ticks ago
     */
    TickDto getByAge(int ageInTicks);


    /**
     * Retrieves all the remembered price points of BTC in the database.
     * @return      list of all the BTC remembered prices
     */
    List<TickDto> getAll();
}
