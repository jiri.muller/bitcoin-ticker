package cz.jirimuller.bitcointicker.service.impl;

import cz.jirimuller.bitcointicker.client.CoindeskClient;
import cz.jirimuller.bitcointicker.client.CoindeskResult;
import cz.jirimuller.bitcointicker.domain.Tick;
import cz.jirimuller.bitcointicker.exception.TickerNotFoundException;
import cz.jirimuller.bitcointicker.mapper.TickMapper;
import cz.jirimuller.bitcointicker.model.TickDto;
import cz.jirimuller.bitcointicker.repository.TickRepository;
import cz.jirimuller.bitcointicker.service.TickerService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Data
@RequiredArgsConstructor
public class TickerServiceImpl implements TickerService {

    private static final Logger log = LoggerFactory.getLogger(TickerServiceImpl.class);
    private final TickRepository repository;
    private final TickMapper mapper;
    private final CoindeskClient client;

    @Value("${app.deleteRecordsAfterHours}")
    public Long deleteAfter;

    @Transactional
    @Override
    public void saveCurrent() {
        log.info("saving the latest bitcoin value");

        CoindeskResult result = client.getResult();
        Tick tick = new Tick().setPrice(result.getData().getBitcoin().getPricesDetail().getPrice())
                .setTimestamp(LocalDateTime.now());
        repository.save(tick);
        repository.deleteAllByTimestampBefore(tick.getTimestamp().minusHours(deleteAfter));

        log.debug("saved a new value: {}", tick);
    }

    @Transactional(readOnly = true)
    @Override
    public TickDto getCurrent() {
        log.info("retrieving the latest bitcoin value");

        final Tick currentTick = repository.getCurrent()
                .orElseThrow(TickerNotFoundException::new);
        final TickDto currentTickDto = mapper.mapToDto(currentTick);

        log.debug("getCurrent returned {}", currentTickDto);

        return currentTickDto;
    }

    @Transactional(readOnly = true)
    @Override
    public TickDto getHighest() {
        log.info("retrieving the highest remembered bitcoin value");

        final Tick highestTick = repository.getHighest()
                .orElseThrow(TickerNotFoundException::new);;
        final TickDto highestTickDto = mapper.mapToDto(highestTick);

        log.debug("getHighest returned {}", highestTickDto);

        return highestTickDto;
    }

    @Transactional(readOnly = true)
    @Override
    public TickDto getLowest() {
        log.info("retrieving the lowest remembered bitcoin value");

        final Tick lowestTick = repository.getLowest()
                .orElseThrow(TickerNotFoundException::new);;
        final TickDto lowestTickDto = mapper.mapToDto(lowestTick);

        log.debug("getLowest returned {}", lowestTickDto);

        return lowestTickDto;
    }

    @Transactional(readOnly = true)
    @Override
    public TickDto getByAge(int ageInTicks) {
        log.info("retrieving bitcoin value by age in ticks");
        log.debug("retrieving bitcoin value {} ticks old", ageInTicks);

        final Tick tickByAge = repository.getByAge(ageInTicks)
                .orElseThrow(TickerNotFoundException::new);
        final TickDto tickDto = mapper.mapToDto(tickByAge);

        log.debug("getByAge returned {}", tickDto);

        return tickDto;
    }

    @Transactional(readOnly = true)
    @Override
    public List<TickDto> getAll() {
        log.info("retrieving all remembered bitcoin values");

        final List<TickDto> ticks = repository.findAll().stream()
                .map(mapper::mapToDto)
                .collect(Collectors.toList());

        log.debug("getAll returned {}", ticks);

        return ticks;
    }
}
