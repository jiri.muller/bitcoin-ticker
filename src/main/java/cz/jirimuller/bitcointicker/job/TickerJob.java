package cz.jirimuller.bitcointicker.job;

import cz.jirimuller.bitcointicker.service.TickerService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
@RequiredArgsConstructor
public class TickerJob {

    private final TickerService service;
    private final Logger log = LoggerFactory.getLogger(TickerJob.class);

    @Transactional
    @Scheduled(cron = "${app.cronJobExpression}")
    public void getCurrentTick() {
        log.debug("getCurrentTick cron job started");
        service.saveCurrent();
    }

}
