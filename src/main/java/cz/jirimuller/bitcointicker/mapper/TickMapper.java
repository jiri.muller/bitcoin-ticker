package cz.jirimuller.bitcointicker.mapper;

import cz.jirimuller.bitcointicker.client.CoindeskResult;
import cz.jirimuller.bitcointicker.domain.Tick;
import cz.jirimuller.bitcointicker.model.TickDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.LocalDateTime;

@Mapper(imports = {LocalDateTime.class})
public interface TickMapper{
    TickDto mapToDto(Tick tick);

    @Mapping(target = "timestamp", constant = "LocalDatetime.now()")
    Tick mapToEntity(CoindeskResult result);
}
