package cz.jirimuller.bitcointicker.client;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.jirimuller.bitcointicker.exception.TickerException;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class CoindeskClient {

    public CoindeskResult getResult() {

        OkHttpClient client = new OkHttpClient();
        ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        Request request = new Request.Builder()
                .url("https://production.api.coindesk.com/v2/tb/price/ticker?assets=all")
                .build();

        Call call = client.newCall(request);
        try {
            Response response = call.execute();
            return objectMapper.readValue(response.body().bytes(), CoindeskResult.class);
        } catch (IOException e) {
            throw new TickerException("Request failed", HttpStatus.NOT_FOUND);
        }
    }
}
