package cz.jirimuller.bitcointicker.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class CoindeskResult {
    private Currencies data;

    @Data
    public static class Currencies {
        @JsonProperty("BTC")
        private CurrencyPrice bitcoin;
    }

    @Data
    public static class CurrencyPrice {
        @JsonProperty("ohlc")
        private CurrencyPricesDetail pricesDetail;
    }

    @Data
    public static class CurrencyPricesDetail {
        @JsonProperty("c")
        private BigDecimal price;
    }
}
