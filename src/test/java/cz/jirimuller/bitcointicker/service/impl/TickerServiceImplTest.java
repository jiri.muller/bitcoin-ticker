package cz.jirimuller.bitcointicker.service.impl;

import cz.jirimuller.bitcointicker.domain.Tick;
import cz.jirimuller.bitcointicker.exception.TickerNotFoundException;
import cz.jirimuller.bitcointicker.mapper.TickMapper;
import cz.jirimuller.bitcointicker.model.TickDto;
import cz.jirimuller.bitcointicker.repository.TickRepository;

import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TickerServiceImplTest implements WithAssertions {

    @Mock
    private TickRepository repository;

    @Mock
    private TickMapper mapper;

    @InjectMocks
    private TickerServiceImpl service;

    @Test
    void getCurrent() {
        // arrange
        final Tick expectedTick = new Tick()
                .setTimestamp(LocalDateTime.now())
                .setId(1L)
                .setPrice(BigDecimal.ONE);

        final TickDto expectedTickDto = new TickDto();

        when(repository.getCurrent())
                .thenReturn(Optional.of(expectedTick));
        when(mapper.mapToDto(expectedTick))
                .thenReturn(expectedTickDto);

        // act
        final TickDto currentTickDto = service.getCurrent();

        // assert
        assertThat(currentTickDto).isNotNull();
        assertThat(currentTickDto).isEqualTo(expectedTickDto);
        verify(mapper).mapToDto(expectedTick);
        verifyNoMoreInteractions(repository, mapper);
    }

    @Test
    void getCurrent_noData() {
        // arrange
        when(repository.getCurrent())
                .thenReturn(Optional.empty());

        // act & assert
        TickerNotFoundException expectedException = assertThrows(TickerNotFoundException.class, () -> service.getCurrent());
        assertThat(expectedException.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        verifyNoMoreInteractions(repository, mapper);
    }

    @Test
    void getHighest() {
        // arrange
        final Tick expectedTick = new Tick()
                .setTimestamp(LocalDateTime.now())
                .setId(1L)
                .setPrice(BigDecimal.ONE);

        final TickDto expectedTickDto = new TickDto();

        when(repository.getHighest())
                .thenReturn(Optional.of(expectedTick));
        when(mapper.mapToDto(expectedTick))
                .thenReturn(expectedTickDto);

        // act
        final TickDto currentTickDto = service.getHighest();

        // assert
        assertThat(currentTickDto).isNotNull();
        assertThat(currentTickDto).isEqualTo(expectedTickDto);
        verify(repository).getHighest();
        verify(mapper).mapToDto(expectedTick);
        verifyNoMoreInteractions(repository, mapper);
    }

    @Test
    void getHighest_noData() {
        // arrange
        when(repository.getHighest())
                .thenReturn(Optional.empty());

        // act & assert
        TickerNotFoundException expectedException = assertThrows(TickerNotFoundException.class, () -> service.getHighest());
        assertThat(expectedException.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        verifyNoMoreInteractions(repository, mapper);
    }

    @Test
    void getLowest() {
        // arrange
        final Tick expectedTick = new Tick()
                .setTimestamp(LocalDateTime.now())
                .setId(1L)
                .setPrice(BigDecimal.ONE);

        final TickDto expectedTickDto = new TickDto();

        when(repository.getLowest())
                .thenReturn(Optional.of(expectedTick));
        when(mapper.mapToDto(expectedTick))
                .thenReturn(expectedTickDto);

        // act
        final TickDto currentTickDto = service.getLowest();

        // assert
        assertThat(currentTickDto).isNotNull();
        assertThat(currentTickDto).isEqualTo(expectedTickDto);
        verify(repository).getLowest();
        verify(mapper).mapToDto(expectedTick);
        verifyNoMoreInteractions(repository, mapper);
    }

    @Test
    void getLowest_noData() {
        // arrange
        when(repository.getLowest())
                .thenReturn(Optional.empty());

        // act & assert
        TickerNotFoundException expectedException = assertThrows(TickerNotFoundException.class, () -> service.getLowest());
        assertThat(expectedException.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        verifyNoMoreInteractions(repository, mapper);
    }

    @Test
    void getAll() {
        // arrange
        final Tick tick0 = new Tick();
        final Tick tick1 = new Tick();
        final Tick tick2 = new Tick();
        final List<Tick> listTicks = List.of(tick0, tick1, tick2);
        final List<TickDto> listTickDtos = listTicks.stream().map(mapper::mapToDto).collect(Collectors.toList());

        when(repository.findAll())
                .thenReturn(listTicks);

        // act
        final List<TickDto> allDtos = service.getAll();

        // assert
        assertThat(allDtos).isNotNull();
        assertThat(allDtos).containsExactlyInAnyOrderElementsOf(listTickDtos);
        verify(repository).findAll();
        verify(mapper, times(6)).mapToDto(any());
        verifyNoMoreInteractions(repository, mapper);
    }

    @Test
    void getAll_noData() {
        // arrange
        final List<Tick> emptyList = new LinkedList<>();

        when(repository.findAll())
                .thenReturn(emptyList);

        // act
        final List<TickDto> emptyResult = service.getAll();

        // assert
        assertThat(emptyResult).isEqualTo(emptyList);
        verify(repository).findAll();
        verifyNoMoreInteractions(mapper, repository);
    }
}
