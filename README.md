# bitcoin-ticker

This is a simple Spring Boot app that polls the public Coindesk API every minute (although any interval can be set in the configuration file) and stores an hour's worth of records (again, this is configurable). These can then be retrieved via the RESTful API endpoints.

The app uses OkHttp3 to connect to the Coindesk API, Hibernate for persistence, and PostgreSQL as the default database provider.
Logging is facilitated by Slf4j and Logback.

## Build instructions

After cloning the repository, run
`docker-compose up`
to run the PostgreSQL container, then
`mvn spring-boot:run`
to build and run the app.

## Features and configuration

The price points that can be retrieved include the most recent, the highest/lowest, older price points specified by their age in ticks, or all remembered price points.

When the app is running, visit http://localhost:8080/swagger-ui.html for a thorough description of the API.

The aforementioned configurable properties can be found in the `application.yml` configuration file:

`app.deleteRecordsAfterHours` specifies how many hours of records are kept; this is just a regular integer.

`app.cronJobExpression` can be used to specify any simple or complex interval compliant with the Spring cron expression syntax, visit [https://spring.io/blog/2020/11/10/new-in-spring-5-3-improved-cron-expressions](url) for more information.
